## Privacy Policy

Rusted Kraft built the Deep Rope app as a Free app. This SERVICE is provided by Rusted Kraft at no cost and is intended for use as is.

This page is used to inform visitors regarding my policies with the collection, use, and disclosure of Personal Information if anyone decided to use my Service.

If you choose to use my Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that I collect is used for providing and improving the Service. I will not use or share your information with anyone except as described in this Privacy Policy.

The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at Deep Rope unless otherwise defined in this Privacy Policy.

**Information We Collect From You**

Most of the information that we collect about you comes directly from you when you play our Apps. In general, the information we collect about you relates to the type of device you are using, information that helps us identify your device, how you play our apps and may include information that you submit to us when you voluntarily contact us or which you allow us to access when you connect to your social network accounts through our Apps.

* Technical Information
This information includes amongst others the type of device(s) you are using to play our Apps, persistent identifiers, such as IP address, device identifiers, unique user ID specific for our Apps, and the country or region that you are playing in.

* Classical Personal Information
We do not collect classical personal information through our Apps (such as you name, address, email or phone number) and you can use our Apps without providing us with your classical personal information. We will receive your classical personal information (such as you name, address, email or phone number) only when you contact us directly and will use such information only to respond to your inquiry.

* Third party services
For a better experience, while using our Service, I may require you to provide us with certain personally identifiable information. The information that I request will be retained on your device and is not collected by me in any way.
The app does use third party services that may collect information used to identify you.
Link to privacy policy of third party service providers used by the app:
    *    [Google Play Services](https://www.google.com/policies/privacy/)
    *    [Unity](https://unity3d.com/legal/privacy-policy)

**Children’s Privacy**

Child Safe Apps

If your Rusted Kraft game is labelled a Child Safe App, it was built with child safety as a priority. Our Child Safe Apps have no advertising, no data collection, no in-app purchases and no links to external sites or apps. 

If you permit your child to use one of our Services that is not labelled a Child Safe App, you should disable in-app purchases on your phone and actively monitor your child’s use. You should also familiarise yourself with this privacy policy, as by permitting your child to use our Services you consent to this privacy policy on their behalf. If you don’t agree, please do not permit your child to use our Services.

We do not knowingly contact or collect information from children under 13 without the permission of their parent/guardians. If you believe that we have inadvertently collected such information, please contact us so we can promptly obtain parental/guardian consent or delete the information.

**Changes to This Privacy Policy**

I may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. I will notify you of any changes by posting the new Privacy Policy on this page. These changes are effective immediately after they are posted on this page.

**Contact Us**

If you have any questions or suggestions about my Privacy Policy, do not hesitate to contact me at deeprope.game@gmail.com.
